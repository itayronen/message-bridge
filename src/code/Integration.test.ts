import { TestSuite, TestParams } from "just-test-api";
import { expect } from "chai";
import { Event1, Event2 } from "itay-events";
import { ServiceToMessageBridge } from "./ServiceToMessageBridge";
import { MessageToServiceBridge } from "./MessageToServiceBridge";
import { ChannelPort, MessageArgs } from "./Channelport";

interface ServiceBridge {
	getTrue(): Promise<boolean>;
	getComplex(): Promise<[number, { a: number }]>;
	and(a: boolean, b: boolean): Promise<boolean>;
	voidFunc(): Promise<void>;
	getNumberAsync(): Promise<number>;
	addAsync(a: number, b: number): Promise<number>;
	doAsync(): Promise<void>;
	throwsString(message: string): Promise<void>;
	throwsError(message: string): Promise<void>;
	throwsErrorAsync(message: string): Promise<void>;

	onEvent1: Event1<number>;
	onEvent2: Event2<number, number>;
}

class Event2Extended<T1, T2> extends Event2<T1, T2>{
}

const COMPLEX: [number, { a: number }] = [5, { a: 6 }];

class Service {
	public getTrue(): boolean { return true; }
	public getComplex(): [number, { a: number }] { return COMPLEX; }
	public and(a: boolean, b: boolean): boolean { return a && b; }
	public voidFunc(): void { }
	public getNumberAsync(): Promise<number> {
		return new Promise(resolve => { setTimeout(() => resolve(100), 1); });
	}
	public addAsync(a: number, b: number): Promise<number> {
		return new Promise(resolve => { setTimeout(() => resolve(a + b), 1); });
	}
	public doAsync(): Promise<void> {
		return new Promise(resolve => { setTimeout(resolve, 1); });
	}
	public throwsString(message: string): void {
		throw message;
	}
	public throwsError(message: string): void {
		throw new Error(message);
	}
	public throwsErrorAsync(message: string): Promise<void> {
		return new Promise((_, reject) => { setTimeout(() => reject(new Error(message)), 1); });
	}

	public onEvent1 = new Event1<number>();
	public onEvent2 = new Event2Extended<number, number>();
}

class FakeChannel {
	public port1: ChannelPort = {
		onmessage: (e: MessageArgs) => { },
		postMessage: (data: string) => { this.port2.onmessage({ data }); }
	};

	public port2: ChannelPort = {
		onmessage: (e: MessageArgs) => { },
		postMessage: (data: string) => { this.port1.onmessage({ data }); }
	};
}

function arrange(service: any = new Service(), channel: FakeChannel = new FakeChannel()): Promise<ServiceBridge> {
	let bridgePromise: Promise<ServiceBridge> = ServiceToMessageBridge.create(channel.port1);
	let workerSideBridge = MessageToServiceBridge.create(service, channel.port2);

	return bridgePromise;
}

export default function (suite: TestSuite): void {
	suite.describe("Integration", suite => {
		suite.test(Service.prototype.getTrue.name, async t => {
			t.arrange();
			let bridge1 = await arrange();

			t.act();
			let result = await bridge1.getTrue();

			t.assert();
			expect(result).to.be.true;
		});

		suite.test(Service.prototype.getComplex.name, async t => {
			t.arrange();
			let bridge1 = await arrange();

			t.act();
			let result = await bridge1.getComplex();

			t.assert();
			expect(result).to.deep.equal(COMPLEX);
		});

		suite.test(Service.prototype.and.name, async t => {
			t.arrange();
			let bridge1 = await arrange();

			t.act();
			let result = await bridge1.and(true, false);

			t.assert();
			expect(result).to.be.false;
		});

		suite.test(Service.prototype.voidFunc.name, async t => {
			t.arrange();
			let bridge1 = await arrange();

			t.act();
			let result = await bridge1.voidFunc();

			t.assert();
			expect(result).to.be.null;
		});

		suite.test(Service.prototype.getNumberAsync.name, async t => {
			t.arrange();
			let bridge1 = await arrange();

			t.act();
			let result = await bridge1.getNumberAsync();

			t.assert();
			expect(result).to.equal(100);
		});

		suite.test(Service.prototype.addAsync.name, async t => {
			t.arrange();
			let bridge1 = await arrange();

			t.act();
			let result = await bridge1.addAsync(4, 9);

			t.assert();
			expect(result).to.equal(13);
		});

		suite.test(Service.prototype.doAsync.name, async t => {
			t.arrange();
			let bridge1 = await arrange();

			t.act();
			let result = await bridge1.doAsync();

			t.assert();
			expect(result).to.be.null;
		});

		suite.test(Service.prototype.throwsString.name, async t => {
			t.arrange();
			let bridge1 = await arrange();

			t.act();
			let error: any;
			try {
				await bridge1.throwsString("asd");
			}
			catch (reason) {
				error = reason;
			}

			t.assert();
			expect(error).to.equal("asd");
		});

		suite.test(Service.prototype.throwsError.name, async t => {
			t.arrange();
			let bridge1 = await arrange();

			t.act();
			let error: any;
			try {
				await bridge1.throwsError("asd");
			}
			catch (reason) {
				error = reason;
			}

			t.assert();
			expect(error).to.be.not.undefined;
			expect(error.message).to.equal("asd");
		});

		suite.test(Service.prototype.throwsErrorAsync.name, async t => {
			t.arrange();
			let bridge1 = await arrange();

			t.act();
			let error: any;
			try {
				await bridge1.throwsErrorAsync("asd");
			}
			catch (reason) {
				error = reason;
			}

			t.assert();
			expect(error).to.be.not.undefined;
			expect(error.message).to.equal("asd");
		});

		suite.test("When broken channel errors on request, then reject with error.", async t => {
			t.arrange();
			let brokenChannel = new FakeChannel();
			brokenChannel.port1.postMessage = () => { throw new Error("asd"); };

			let bridge1 = await arrange(undefined, brokenChannel);

			t.act();
			let error: any;
			try {
				await bridge1.doAsync();
			}
			catch (reason) {
				error = reason;
			}

			t.assert();
			expect(error).to.be.not.undefined;
			expect(error.message).to.equal("asd");
		});

		suite.test("When service class lambda functions (after compilation, they are created in the ctor). " +
			"This will fail if the functions map relies on the prototype functions." +
			"This was a bug in an older implementation.", async t => {
				t.arrange();
				class ServiceWithLambda {
					public getTrue(): boolean { return true; }
					private lamdaFunc = () => { };
				}

				let bridge1 = await arrange(new ServiceWithLambda());

				t.act();
				let result = await bridge1.getTrue();

				t.assert();
				expect(result).to.be.true;
			});
	});

	suite.test("onEvent1", async t => {
		t.arrange();
		let service = new Service();
		let bridge = await arrange(service);

		t.act();
		let result = 0;
		bridge.onEvent1.add(e => result = e);

		service.onEvent1.trigger(3);

		t.assert();
		expect(result).to.equal(3);
	});

	suite.test("onEvent2", async t => {
		t.arrange();
		let service = new Service();
		let bridge = await arrange(service);

		t.act();
		let result1 = 0;
		let result2 = 0;
		bridge.onEvent2.add((a, b) => { result1 = a; result2 = b; });

		service.onEvent2.trigger(3, 4);

		t.assert();
		expect(result1).to.equal(3);
		expect(result2).to.equal(4);
	});
}