import { Event1 as OnlyForTypeEvent1, Event2 as OnlyForTypeEvent2 } from "itay-events";
import { ChannelPort, MessageArgs } from "./Channelport";
import {
	InterfaceMessage, ServiceMessage,
	RequestFunction, FunctionResponse, Event1Triggered, Event2Triggered,
	EventId, MessageId, FunctionId, SUCCESS, ERROR
} from "./CommunicationTypes";

type Event1<T> = OnlyForTypeEvent1<T>;
type Event2<T1, T2> = OnlyForTypeEvent2<T1, T2>;

var Event1: typeof OnlyForTypeEvent1;
var Event2: typeof OnlyForTypeEvent2;

interface PromiseController {
	resolve: (data?: any) => void;
	reject: (reason: any) => void;
}

export class ServiceToMessageBridge {
	private messageId = 0;
	private messageIdToPromiseMap = new Map<MessageId, PromiseController>();

	private event1IdToEvent1Map = new Map<EventId, Event1<any>>();
	private event2IdToEvent2Map = new Map<EventId, Event2<any, any>>();

	private requestArr: RequestFunction = [0, 0, null!];

	private bridgeObject: any;
	private bridgeObjectServiceToMessageSymbol = Symbol(ServiceToMessageBridge.name);

	private bindPromiseController!: PromiseController;
	private bindPromise: Promise<void>;

	private constructor(private channel: ChannelPort) {
		this.bridgeObject = {};
		this.bridgeObject[this.bridgeObjectServiceToMessageSymbol] = this;

		this.bindPromise = new Promise((resolve, reject) => {
			this.bindPromiseController = { resolve, reject };
		});

		this.channel.onmessage = this.handleInterfaceMessage;
	}

	public static async create<T>(channel: ChannelPort): Promise<T> {
		let serviceToMessage = new ServiceToMessageBridge(channel);
		await serviceToMessage.bindPromise;

		return serviceToMessage.bridgeObject as any;
	}

	private handleInterfaceMessage = async (e: MessageArgs) => {
		try {
			let interfaceMessage: InterfaceMessage = JSON.parse(e.data);

			this.createFunctionsBridges(interfaceMessage.function);

			if (interfaceMessage.event1.length + interfaceMessage.event2.length > 0) {
				await this.importItayEvents();
				this.createEvent1Bridges(interfaceMessage.event1);
				this.createEvent2Bridges(interfaceMessage.event2);
			}

			this.channel.onmessage = this.handleMessage;

			this.bindPromiseController.resolve();
		}
		catch (reason) {
			this.bindPromiseController.reject(reason);
			this.channel.onmessage = undefined!;
		}
	}

	private async importItayEvents(): Promise<void> {
		if (!Event1) {
			let itayEvents = await import("itay-events");
			Event1 = itayEvents.Event1;
			Event2 = itayEvents.Event2;
		}
	}

	private createEvent1Bridges(keys: string[]): void {
		let eventId = 0;

		for (let key of keys) {
			this.bridgeObject[key] = this.createBridgeEvent1(eventId++, this.channel);
		}
	}

	private createEvent2Bridges(keys: string[]): void {
		let eventId = 0;

		for (let key of keys) {
			this.bridgeObject[key] = this.createBridgeEvent2(eventId++, this.channel);
		}
	}

	private createBridgeEvent1(eventId: EventId, channel: ChannelPort): Event1<any> {
		let event = new Event1<any>();
		this.event1IdToEvent1Map.set(eventId, event);

		return event;
	}

	private createBridgeEvent2(eventId: EventId, channel: ChannelPort): Event2<any, any> {
		let event = new Event2<any, any>();
		this.event2IdToEvent2Map.set(eventId, event);

		return event;
	}

	private createFunctionsBridges(keys: string[]): void {
		let funcMessageId = 0;

		for (let key of keys) {
			this.bridgeObject[key] = this.createBridgeFunction(funcMessageId++, this.channel);
		}
	}

	private createBridgeFunction(funcId: FunctionId, channel: ChannelPort): any {
		return (...args: any[]) => this.requestFunction(funcId, args);
	}

	private requestFunction(funcId: FunctionId, args: any[]): Promise<any> {
		this.requestArr[0] = ++this.messageId;
		this.requestArr[1] = funcId;
		this.requestArr[2] = args;

		let promise = new Promise<any>((resolve, reject) => {
			this.messageIdToPromiseMap.set(this.messageId, { resolve, reject });
		});

		this.channel.postMessage(JSON.stringify(this.requestArr));

		return promise;
	}

	private handleMessage = (e: MessageArgs) => {
		let arr: ServiceMessage = JSON.parse(e.data);

		switch (arr[0]) {
			case 0:
				this.handleFunctionResponse(arr as FunctionResponse);
				break;

			case 1:
				this.handleEvent1(arr as Event1Triggered);
				break;

			case 2:
				this.handleEvent2(arr as Event2Triggered);
				break;

			default:
				console.error(`Unknown message type: ${arr[0]}`);
		}

	}

	private handleEvent1(arr: Event1Triggered): void {
		let event: Event1<any> = this.event1IdToEvent1Map.get(arr[1])!;
		event.trigger(arr[2]);
	}

	private handleEvent2(arr: Event2Triggered): void {
		let event: Event2<any, any> = this.event2IdToEvent2Map.get(arr[1])!;
		event.trigger(arr[2], arr[3]);
	}

	private handleFunctionResponse(arr: FunctionResponse): void {
		let messageId = arr[1];

		let promiseController = this.messageIdToPromiseMap.get(messageId)!;
		this.messageIdToPromiseMap.delete(messageId);

		if (arr[2] === SUCCESS) {
			promiseController.resolve(arr[3]);
		}
		else {
			promiseController.reject(arr[3]);
		}
	}
}