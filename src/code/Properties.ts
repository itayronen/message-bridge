export function* getKeys(obj: any): IterableIterator<string> {
	for (let key of Object.getOwnPropertyNames(obj)) {
		if (key === "constructor") continue;

		yield key;
	}

	let proto = Object.getPrototypeOf(obj);

	if (proto != Object.prototype && proto != Function.prototype) {
		yield* getKeys(proto);
	}
}

export interface KeysByTypes {
	function: string[];
	event1: string[];
	event2: string[];
	unrecognized: string[];
}

export function getKeysGroupedByTypes(obj: any): KeysByTypes {
	let result: KeysByTypes = { function: [], event1: [], event2: [], unrecognized: [] };

	for (let key of getKeys(obj)) {
		let value = obj[key];

		if (typeof value === "function") {
			result.function.push(key);
			continue;
		}

		if (isEvent(value)) {
			if (value.trigger.length === 1) {
				result.event1.push(key);
				continue;
			}

			if (value.trigger.length === 2) {
				result.event2.push(key);
				continue;
			}
		}

		result.unrecognized.push(key);
	}

	return result;
}

function isEvent(obj: any): boolean {
	return typeof obj === "object" &&
		typeof obj.add === "function" &&
		typeof obj.trigger === "function";
}