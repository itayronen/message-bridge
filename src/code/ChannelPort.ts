export interface MessageArgs {
	readonly data: any;
}

export interface ChannelPort {
	onmessage: (e: MessageArgs) => void;
	postMessage(message: string): void;
}