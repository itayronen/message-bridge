export type FunctionId = number;
export type EventId = number;
export type MessageId = number;

export const SUCCESS = 0;
export const ERROR = 1;

export type SuccessType = 0;
export type ErrorType = 1;

export type Status = SuccessType | ErrorType;
export type Result = any;
export type Reason = any;

export interface InterfaceMessage {
	function: string[];
	event1: string[];
	event2: string[];
}

export interface RequestFunction {
	[0]: MessageId;
	[1]: FunctionId;
	[2]: ReadonlyArray<any>;
}

export type ServiceMessage = FunctionResponse | Event1Triggered | Event2Triggered;

export interface FunctionResponse {
	[0]: 0;
	[1]: MessageId;
	[2]: Status;
	[3]: Result | Reason;
}

export interface Event1Triggered {
	[0]: 1;
	[1]: EventId;
	[2]: any;
}

export interface Event2Triggered {
	[0]: 2;
	[1]: EventId;
	[2]: any;
	[3]: any;
}