import { Event1, Event2 } from "itay-events";
import { ChannelPort, MessageArgs } from "./ChannelPort";
import { getKeysGroupedByTypes, KeysByTypes } from "./Properties";
import {
	InterfaceMessage, RequestFunction, FunctionResponse, Event1Triggered, Event2Triggered,
	EventId, MessageId, FunctionId, SUCCESS, ERROR
} from "./CommunicationTypes";

interface MessageHandler {
	(messageId: MessageId, args: ReadonlyArray<any>): void;
}

export class MessageToServiceBridge {
	private funcIdToHandlerMap = new Map<FunctionId, MessageHandler>();
	private responseArr: FunctionResponse = [0, 0, 0, null];
	private event1Arr: Event1Triggered = [1, 0, null];
	private event2Arr: Event2Triggered = [2, 0, null, null];
	private keysByType: KeysByTypes;

	private constructor(private obj: any, private channel: ChannelPort) {
		this.channel.onmessage = this.handleRequest;

		this.keysByType = getKeysGroupedByTypes(this.obj);

		this.createFunctionRequestHandlers(this.keysByType.function);
		this.createEvent1Handlers(this.keysByType.event1);
		this.createEvent2Handlers(this.keysByType.event2);

		this.sendInterfaceMessage();
	}

	public static create<T>(obj: any, channel: ChannelPort): T {
		return new MessageToServiceBridge(obj, channel) as any;
	}

	private createEvent1Handlers(keys: string[]): void {
		let eventIdCount = 0;

		for (let key of keys) {
			let event: Event1<any> = this.obj[key];
			let eventId = eventIdCount++;

			event.add(arg => {
				this.postEvent1(eventId, arg);
			});
		}
	}

	private createEvent2Handlers(keys: string[]): void {
		let eventIdCount = 0;

		for (let key of keys) {
			let event: Event2<any, any> = this.obj[key];
			let eventId = eventIdCount++;

			event.add((arg1, arg2) => {
				this.postEvent2(eventId, arg1, arg2);
			});
		}
	}

	private postEvent1(eventId: number, arg: any): void {
		this.event1Arr[1] = eventId;
		this.event1Arr[2] = arg;

		this.channel.postMessage(JSON.stringify(this.event1Arr));
	}

	private postEvent2(eventId: number, arg1: any, arg2: any): void {
		this.event2Arr[1] = eventId;
		this.event2Arr[2] = arg1;
		this.event2Arr[3] = arg2;

		this.channel.postMessage(JSON.stringify(this.event2Arr));
	}

	private sendInterfaceMessage(): void {
		let message: InterfaceMessage = this.keysByType;

		this.channel.postMessage(JSON.stringify(message));
	}

	private handleRequest = (e: MessageArgs) => {
		let arr: RequestFunction = JSON.parse(e.data);

		let handler: MessageHandler = this.funcIdToHandlerMap.get(arr[1])!;
		handler(arr[0], arr[2]);
	}

	private createFunctionRequestHandlers(functionsKeys: string[]): void {
		let id = 0;

		for (let key of functionsKeys) {
			let func = this.obj[key];
			this.funcIdToHandlerMap.set(id++, this.createFunctionRequestHandler(func));
		}
	}

	private createFunctionRequestHandler(func: Function): MessageHandler {
		func = func.bind(this.obj);

		return (messageId, args) => {
			try {
				let result: any = func(...args);
				this.handleResult(messageId, result);
			}
			catch (reason) {
				this.postError(messageId, reason);
			}
		};
	}

	private handleResult(messageId: MessageId, result: any): void {
		if (!result || typeof result.then !== "function") {
			this.postResult(messageId, result);
		}
		else {
			result.then(
				(result: any) => {
					this.postResult(messageId, result);
				},
				(reason: any) => {
					this.postError(messageId, reason);
				});
		}
	}

	private postResult(id: MessageId, result: any): void {
		this.responseArr[1] = id;
		this.responseArr[2] = SUCCESS;
		this.responseArr[3] = result;

		this.channel.postMessage(JSON.stringify(this.responseArr));
	}

	private postError(id: MessageId, reason: any): void {
		this.responseArr[1] = id;
		this.responseArr[2] = ERROR;
		this.responseArr[3] = reason;

		this.channel.postMessage(JSON.stringify(this.responseArr, this.stringifyReplaceErrors));
	}

	private stringifyReplaceErrors(key: string, value: any) {
		if (value instanceof Error) {
			var error: any = {};

			for (let key of Object.getOwnPropertyNames(value)) {
				error[key] = (value as any)[key];
			}

			return error;
		}

		return value;
	}
}